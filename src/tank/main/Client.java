/**
 * @Title: Client.java
 * @Package tank.main
 * @Description: 
 * @author adamjwh
 * @date 2018年8月11日
 * @version V1.0
 */
package tank.main;

import tank.client.ClientView;

/**
 * @ClassName: Client
 * @Description: 
 * @author adamjwh
 * @date 2018年8月11日
 *
 */
public class Client {

	public static void main(String[] args){
		new ClientView();
	}
	
}
