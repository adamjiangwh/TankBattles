/**
 * @Title: Server.java
 * @Package tank.main
 * @Description: 
 * @author adamjwh
 * @date 2018年8月11日
 * @version V1.0
 */
package tank.main;

import tank.server.ServerView;

/**
 * @ClassName: Server
 * @Description: 
 * @author adamjwh
 * @date 2018年8月11日
 *
 */
public class Server {
	
	public static void main(String[] args){
		new ServerView();
	}
}
